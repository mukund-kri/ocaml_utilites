with import <nixpkgs> {};

stdenv.mkDerivation {

  name = "ocaml_utilites";
  
  buildInputs = [
    m4
    ncurses

    fswatch
  ] ++ (
    with ocamlPackages; [
      ocaml
      
      ninja
      merlin
      opam
      utop
    ]
  );

  shellHook = ''
    eval $(opam env)  
  '';

}
