import sys;import site;import functools;sys.argv[0] = '/nix/store/swk3np477sfr0i8w51ml3k1yqdiwysby-python3.7-cookiecutter-1.6.0/bin/cookiecutter';functools.reduce(lambda k, p: site.addsitedir(p, k), ['/nix/store/swk3np477sfr0i8w51ml3k1yqdiwysby-python3.7-cookiecutter-1.6.0/lib/python3.7/site-packages','/nix/store/f3rbzm84q1bqkyrnarlfni164k71gydv-python3.7-Jinja2-2.10.1/lib/python3.7/site-packages','/nix/store/4ng7jcyykzcrprbxlgvyfavygbicgzk2-python3.7-MarkupSafe-1.1.0/lib/python3.7/site-packages','/nix/store/gzp169g8012kmrwbl2iyssyli3l73dgq-python3.7-setuptools-40.8.0/lib/python3.7/site-packages','/nix/store/shbkbv0qdvc2hzbhwsb0nsis7bz9708i-python3.7-future-0.17.1/lib/python3.7/site-packages','/nix/store/saxp0sbyx7njlvfgkmd4q9rrpg8bsxqc-python3.7-binaryornot-0.4.4/lib/python3.7/site-packages','/nix/store/sk8x7ik7pxij9bjw0w3jmn7akyv5r60x-python3.7-chardet-3.0.4/lib/python3.7/site-packages','/nix/store/8cqpibf95abc2wiqk6vdjk9mm2fzgzc8-python3.7-click-7.0/lib/python3.7/site-packages','/nix/store/la3hpk1xcv0igrrkh5ai01ljljnbk14j-python3.7-whichcraft-0.5.2/lib/python3.7/site-packages','/nix/store/avi96slj6j3459v79rj4b5asp2h1nr1z-python3.7-poyo-0.4.2/lib/python3.7/site-packages','/nix/store/n6h1dz2m4gcpnkhyk4frlwfm0djhpi28-python3.7-jinja2-time-0.2.0/lib/python3.7/site-packages','/nix/store/jbf3av2vli7i3rp0djypnzh8ikl5zabk-python3.7-arrow-0.13.1/lib/python3.7/site-packages','/nix/store/q0zwzkrz8905k935s52jnscqzna0jswp-python3.7-python-dateutil-2.8.0/lib/python3.7/site-packages','/nix/store/bs4jw5gkdfa1i8nfk2ikicbac9c9an0q-python3.7-six-1.12.0/lib/python3.7/site-packages','/nix/store/zr08szpaillsmmq8z9sbvhp3cg7hqalk-python3.7-setuptools_scm-3.2.0/lib/python3.7/site-packages','/nix/store/r64yi9v0xm6hsh0dk8yi4b60d9pzb2c6-python3.7-requests-2.21.0/lib/python3.7/site-packages','/nix/store/4fpn1zlw1amyi8v1c5386gnamw4z6l0p-python3.7-urllib3-1.24.1/lib/python3.7/site-packages','/nix/store/z2423i78na58m5mmr7fmz4anmsjwgk1a-python3.7-asn1crypto-0.24.0/lib/python3.7/site-packages','/nix/store/2i68mad623pzvnpj0c0hkghj6rjp93dg-python3.7-packaging-19.0/lib/python3.7/site-packages','/nix/store/81m3mahkzivk84cv4sqfyk2axqdpl2w1-python3.7-pyparsing-2.3.1/lib/python3.7/site-packages','/nix/store/s62zswr6j1kq7ki5d7j6pmb12bqrmghc-python3.7-pycparser-2.19/lib/python3.7/site-packages','/nix/store/vf4fi7i37fpzddw6zg40kz7z3g245r48-python3.7-cffi-1.12.1/lib/python3.7/site-packages','/nix/store/f0v9jkxjrqhn0h6s178p1hfjvlwgzgkc-python3.7-cryptography-2.5/lib/python3.7/site-packages','/nix/store/237f4ivqffs47sf0gigy862zxc6sfhff-python3.7-pyasn1-0.4.5/lib/python3.7/site-packages','/nix/store/i209f7fdxp2xv78nypvy24ikzqvnb1c1-python3.7-idna-2.8/lib/python3.7/site-packages','/nix/store/s58qn2k2rwgkbi9jyn8spv3nn3pzl2cy-python3.7-pyOpenSSL-19.0.0/lib/python3.7/site-packages','/nix/store/2bjc3krrkg8zm1bck4g6ngaaz5apc54k-python3.7-certifi-2018.11.29/lib/python3.7/site-packages','/nix/store/nk9xch8jiq2ivy1fyw93q0ls2791ijhg-python3.7-pysocks-1.6.8/lib/python3.7/site-packages'], site._init_pathinfo());
import re
import sys


from cookiecutter.main import cookiecutter


def mk_ocaml_day(folder):
    cookiecutter(
        '/home/mukund/my-cookiecutters/ocaml-simple/',
        no_input=True,
        extra_context={ 'project': folder }
    )

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("You need to supply the folder name")
        sys.exit(1)
        
    dir_name = sys.argv[1]
    mk_ocaml_day(dir_name)
