open Core 
open Printf 
open CalendarLib


(* The whole config will be stored in a record *)
type config = {
  start_date: Date.t;
  offset: int;
}


(* The challenge day number *)
let day_number ?(today = Date.today ()) config =
  let diff = Date.sub today config.start_date in
  (Date.Period.safe_nb_days diff - config.offset)


(* Extract the date from a - seperated format *)
let parse_date_simple str =
  let nums = str
             |> String.split ~on:'-'
             |> List.map ~f:int_of_string in
  Date.make (List.nth_exn nums 0) (List.nth_exn nums 1) (List.nth_exn nums 2)


(* Read the config file stored in ~/.config folder and convert into 
   a config record *)
let read_config () =
  let json = Yojson.Basic.from_file "/home/mukund/.config/ocaml-day.json" in

  Yojson.Basic.Util.(
    let start_date = json |> member "start-date" |> to_string |> parse_date_simple
    and offset = json |> member "offset" |> to_int in
    { start_date; offset }
  )


(* run the python script that runs cookiecutter to generate an OCaml
   project.
*)
let gen_project name =
  let config = read_config () in
  let fn = sprintf "%03d-%s" (day_number config) name in
  Shexp_process.(
    eval (run "python3" ["/home/mukund/utils/bin/ccdriver.py"; fn])
  )

(* command line processing *)
let command =
  Command.basic
    ~summary: "Generate today's challenge project stub"
    Command.Param.(
      map (anon ("name" %: string))
        ~f:(fun name -> (fun () -> gen_project name))
    )


let () =
  Command.run ~version:"1.0" ~build_info:"build-1" command

