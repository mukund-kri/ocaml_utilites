# ocaml_utilites

A bunch of utilities written in ocaml that makes my life easy


## OCaml day project gen

   Generates an ocaml + nix project using cookiecutter
   
### Installation


### Config file

This program needs a json config file in the path
`./.config/ocaml-day.json`.

Right now its very simple ...

```json
{
  "start-date": "2019-8-18"
}

```

### Dependencies

1. shexp 
2. yojson
3. calendar

All of these are opam installable
